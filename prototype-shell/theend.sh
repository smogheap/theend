#!/bin/bash


# check requirements

export DIALOGRC=""
which dialog clear
if [ $? -ne 0 ]; then
	cat <<EOF
the following programs are required in order to play "The End":
	which
	dialog
	clear
EOF
	exit 1
fi


# setup

DIALOG=`which dialog`
CLEAR=`which clear`

mkdir -p ~/.theend
cat <<EOF >~/.theend/dialogrc-ui
aspect = 0
separate_widget = ""
tab_len = 0
visit_items = OFF
use_shadow = ON
use_colors = ON
screen_color = (yellow,black,OFF)
shadow_color = (white,black,OFF)
dialog_color = (white,green,on)
title_color = (YELLOW,green,ON)
border_color = (white,green,on)
button_active_color = (WHITE,magenta,ON)
button_inactive_color = (white,green,on)
button_key_active_color = (yellow,magenta,on)
button_key_inactive_color = (yellow,green,on)
button_label_active_color = (white,magenta,on)
button_label_inactive_color = (white,green,on)
inputbox_color = (BLUE,WHITE,OFF)
inputbox_border_color = border_color
searchbox_color = (YELLOW,WHITE,ON)
searchbox_title_color = (WHITE,WHITE,ON)
searchbox_border_color = (RED,WHITE,OFF)
position_indicator_color = button_key_inactive_color
menubox_color = dialog_color
menubox_border_color = border_color
item_color = dialog_color
item_selected_color = screen_color
tag_color = title_color
tag_selected_color = screen_color
tag_key_color = button_key_inactive_color
tag_key_selected_color = (RED,BLUE,ON)
check_color = dialog_color
check_selected_color = (WHITE,CYAN,ON)
uarrow_color = (GREEN,CYAN,ON)
darrow_color = uarrow_color
itemhelp_color = (black,yellow,off)
form_active_text_color = inputbox_color
form_text_color = (CYAN,BLUE,ON)
form_item_readonly_color = (CYAN,WHITE,ON)
gauge_color = (BLUE,WHITE,ON)
border2_color = dialog_color
inputbox_border2_color = dialog_color
searchbox_border2_color = dialog_color
menubox_border2_color = dialog_color
EOF

cat <<EOF >~/.theend/help.txt
HALP!!
EOF


# introduction

cat <<EOF >~/.theend/tmp
You are the last human alive.
EOF
$DIALOG --backtitle "The End" --title "Introduction" --exit-label "Next" --hfile ~/.theend/help.txt --hline "ENTER to continue" --textbox ~/.theend/tmp 10 44

cat <<EOF >~/.theend/tmp
Whatever happened to the SUN and EARTH,
it seems vastly unlikely there would be
any other surivors, and less likely
still that you might ever find them.
EOF
$DIALOG --backtitle "The End" --title "Introduction" --exit-label "Next" --textbox ~/.theend/tmp 10 44

cat <<EOF >~/.theend/tmp
For the last two days, you've been
tracking a strong signal.  Your hopes
are that this signal's source is
intelligent life, that said life is
friendly, and that a friend of such
intelligent life might find some long-
needed supplies and repairs.

It's rather a tall stack of hopes, when
you lay it all out that way.

You have had no luck understanding the
signal.  Your ship's main computer has
not picked out any patterns either, not
that you had the foresight to load up
on specialized cypher recognition and
deep signal analysis software before
you were --

No time to worry about that right now.
The ship's computer just dropped to a
primitive command menu you've never
seen before.  Last time this computer
reset, you were nearly killed.

You study the main terminal...
EOF
$DIALOG --backtitle "The End" --title "Introduction" --exit-label "Begin" --hfile ~/.theend/help.txt --hline "UP/DOWN arrows to scroll" --textbox ~/.theend/tmp 20 44


# submenu functions

function diag {
	while :; do
		$DIALOG \
			--backtitle "The End" --title "Diagnostics" \
			--hfile ~/.theend/help.txt --item-help --menu \
			"diagnostic module v0.1.1" 12 44 \
			5 \
			full "Full" "Perform all subsystem checks" \
			pow  "Power" "Check power systems" \
			life "Life" "Check life-support systems" \
			comm "Communications" "Check dock, radio, laser tx/rx systems" \
			stor "Data Storage" "Check data operations and integrity" \
			2>~/.theend/tmp
		if [ $? -ne 0 ]; then
			return
		fi
		choice=`cat ~/.theend/tmp`
		case "$choice" in
			"full")
				(
					for chk in "power" "life support"; do
						for i in {1..10}; do
							sleep 1
							cat <<-EOF
							XXX
							${i}
							Checking ${chk}...
							EOF
						done
					done
				) | $DIALOG --gauge "Performing all diagnostics..." 6 64
				;;
		esac
	done
}


# main loop

export DIALOGRC=~/.theend/dialogrc-ui
while :; do
	$DIALOG \
		--backtitle "The End" --title "Main Menu" \
		--cancel-label "Exit" \
		--hfile ~/.theend/help.txt --item-help --menu \
		"smogheap vesselOS v0.1" 12 44 \
		5 \
		diag "Diagnostics" "Perform system diagnostics" \
		resc "Rescue Utility" "Repair and boot to a damaged system" \
		log "System Log" "Review and/or expunge logged events" \
		cust "Customize Settings" "Set interface appearance and behaviour" \
		eras "Erase Data" "Clear unneeded or corrupted data blocks" \
	2>~/.theend/tmp
	if [ $? -ne 0 ]; then
		export DIALOGRC=""
		$DIALOG --backtitle "The End" --yesno \
				"Exit the game and remain stranded in space?" 8 30
		if [ $? -eq 0 ]; then
		   break
		fi
		export DIALOGRC=~/.theend/dialogrc-ui
	fi
	choice=`cat ~/.theend/tmp`
	case "$choice" in
		"diag")
			diag
		;;
	esac
done


# cleanup

rm ~/.theend/tmp
rm ~/.theend/dialogrc-ui
$CLEAR
echo "Thank you for playing!"
